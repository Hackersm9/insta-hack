# Insta-Hack by HackerSM9
![Discription](https://socialify-pt57hdgtz-whe.vercel.app/HackerSM9/insta-hack/image?description=1&forks=1&issues=1&language=1&name=1&owner=1&pattern=Circuit%20Board&pulls=1&stargazers=1&theme=Dark)
## ⭐ If Like ..!!
# Disclaimer:
> **_Note_**: You shall not misuse the information to gain unauthorised access. I will not be Responsible for Anything, Use at Your Own Risk..⚠️ 

> **_Note_**: This not for **illegal usage..⚠️**

## About tool
All in one Instagram hacking tool available (Insta information gathering, Insta brute force, Insta account auto repoter)

## Features:

- Insta information gathering
- Insta brute force attack
- Insta auto repoter
- Update script
- Remove script

## Requirements
- Data connection
- Internet 200MB
- storage 400MB
- No Root

## Available On
- Termux
- Kali Linux

## Test On:
- Termux
- ANDROID 6+

## INSTALLATION [Termux]

```
 apt update
 apt upgrade
 pkg install python
 pkg install python3
 pkg install git
 git clone https://github.com/HackerSM9/insta-hack
 ls
 cd insta-hack
 pip3 install -r requirements.txt
 chmod +x *
 bash setup
```

## INSTALLATION [Kali Linux]

```
 sudo apt install python
 sudo apt install python3
 sudo apt install git
 git clone https://github.com/HackerSM9/insta-hack
 ls
 cd insta-hack
 pip3 install -r requirements.txt
 chmod +x *
 sudo python3 insta-hack.py
```

## Screenshot:
<br>
<p align="center">
<img width="95%" src="https://github.com/HackerSM9/insta-hack/blob/main/Ig_information_gathering/Core_files/HackerSM9_insta_hack.jpg?sanitize=true"></p>
<img width="95%" src="https://github.com/HackerSM9/insta-hack/blob/main/Ig_information_gathering/Core_files/Id_insta.jpg?sanitize=true"\>
